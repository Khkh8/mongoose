# Mongoose assignment
### Henri Vatto
#### Available at https://gitlab.com/khkh8/mongoose
## Implemented features
There are GET, POST, PUT and DELETE requests implemented for all the routes. They all have error handling with TryCatch statements. The data directory has backups of the original data with the correct "_id" values. The postman collection is exported as a json file. I have implemented all features required for a grade 5.

## How to run the application
First a mongodb database is required. The database needs to have the following collections: `authors`, `bookinstances`, `books`, `genres`. Import the corresponding **backup** json files from the data directory to the collections.

The application requires a `.env` file to be created with a port and the mongodb connection string.

Run the command `npm install` in the project root directory to install required dependencies. 

To start the application run the command `npm run start`

The paths for the requests are: `http://localhost:<PORT>/api/v1/<ROUTE>`


# Screenshots
## Postman from Genres
![](./images/imageGenresGet.png)
![](./images/imageGenresPost.png)
![](./images/imageGenresPut.png)
![](./images/imageGenresDelete.png)

## Postman from Authors
![](./images/imageAuthorsGet.png)
![](./images/imageAuthorsPost.png)
![](./images/imageAuthorsPut.png)
![](./images/imageAuthorsDelete.png)

## Postman from Books
![](./images/imageBooksGet.png)
![](./images/imageBooksPost.png)
![](./images/imageBooksPut.png)
![](./images/imageBooksDelete.png)

## Postman from Bookinstances
![](./images/imageBookinstancesGet.png)
![](./images/imageBookinstancesPost.png)
![](./images/imageBookinstancesPut.png)
![](./images/imageBookinstancesDelete.png)