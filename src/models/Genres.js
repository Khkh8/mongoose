const mongoose = require("mongoose")
//Genre
const genreSchema = new mongoose.Schema({
    name: { type: String, require: true, minlength: 3, maxlength: 100 }
})


genreSchema.set("toJSON", {
    transform: (document, returnedObject) => {
        returnedObject.id = returnedObject._id.toString()
        delete returnedObject._id
        delete returnedObject.__v
    }
})

mongoose.set("useCreateIndex", true)
mongoose.set("useFindAndModify", false)

module.exports = mongoose.model("Genre", genreSchema)