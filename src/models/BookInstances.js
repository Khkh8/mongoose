const mongoose = require("mongoose")
const Book = require("./Books")

const bookinstanceSchema = new mongoose.Schema({
    book:     {type: mongoose.Schema.Types.ObjectId, ref: "Book", required: true},
    imprint: {type: String},
    status: {type: String, required: true},
    due_back: {type: Date}
})
bookinstanceSchema.set('toJSON', {
    transform: (document, returnedObject) => {
        returnedObject.id = returnedObject._id.toString();
        delete returnedObject._id;
        delete returnedObject.__v;
    },
});

mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);

module.exports = mongoose.model('BookInstance', bookinstanceSchema)