const mongoose = require("mongoose")

const authorSchema = new mongoose.Schema({
    first_name:    { type: String, required: true, maxlength: 100 },
    family_name:   { type: String, required: true, maxlength: 100 },
    date_of_birth: { type: Date },
    date_of_death: { type: Date }
})
authorSchema.set('toJSON', {
    transform: (document, returnedObject) => {
        returnedObject.id = returnedObject._id.toString();
        delete returnedObject._id;
        delete returnedObject.__v;
    },
});

mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
      
//Export model
module.exports = mongoose.model('Author', authorSchema);