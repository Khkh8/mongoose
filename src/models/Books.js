const mongoose = require("mongoose")
const Genre = require("./Genres")
const Author = require("./Authors")

const bookSchema = new mongoose.Schema({
    title:      {type: String, required: true, maxlength:100},
    author:     {type: mongoose.Schema.Types.ObjectId, ref: "Author", required: true},
    summary:    {type: String, required: true},
    ISBN:       {type: String, required: true},
    genre:      [{type: mongoose.Schema.Types.ObjectId, ref: "Genre", required: true}]
})

bookSchema.set("toJSON", {
    transform: (document, returnedObject) => {
        returnedObject.id = returnedObject._id.toString()
        delete returnedObject._id
        delete returnedObject.__v
    }
})

mongoose.set("useCreateIndex", true)
mongoose.set("useFindAndModify", false)

module.exports = mongoose.model("Book", bookSchema)