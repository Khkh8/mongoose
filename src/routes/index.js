// ./routes/index.js
const genre = require("./genre")
const book = require("./book")
const author = require("./author")
const bookinstance = require("./bookinstance")

module.exports = {
    genre,
    book,
    author,
    bookinstance
}