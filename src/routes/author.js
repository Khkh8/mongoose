const router = require("express").Router()
const Author = require("../models/Authors")

router.get("/", async (req, res) => {
    try {
        const authors = await Author.find().lean()
        res.send(authors);
    }
    catch (e) {
        console.log("Error in get authors + " + e)
        res.status(404).send(e)
    }
})

router.get("/:id?", async (req, res) => {
    try {
        const authors = await Author.find({ _id: req.params.id }).lean()
        res.send(authors);
    }
    catch (e) {
        console.log("Error in get authors by id + " + e)
        res.status(404).send(e)
    }
})
router.post("/", async (req, res) => {
    try {
        const authors = await Author.find({ first_name: req.body.first_name }).lean()
        res.send(authors);
    } catch (e) {
        console.log("Error in post authors + " + e);
        res.status(404).send(e)
    }
})

router.put("/:id?", async (req, res) => {
    try {
        await Author.findByIdAndUpdate(req.params.id, req.body, function (err, doc) {
            if (err) return res.send(500, { error: err })
            return res.send("Changed succesfully")
        })
    } catch (e) {
        console.log("Error in put authors + " + e);
        res.status(404).send(e)
    }
})

router.delete("/:id?", async (req, res) => {
    try {
        await Author.findByIdAndDelete(req.params.id, function (err, doc) {
            if (err) return res.send(500, { error: err })
            return res.send("Deleted succesfully")
        })
    } catch (e) {
        console.log("Error in delete authors + " + e);
        res.status(404).send(e)
    }
})

module.exports = router