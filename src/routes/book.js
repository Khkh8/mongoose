const router = require("express").Router()
const Book = require("../models/Books")

router.get("/", async (req, res) => {
    try {
        const books = await Book.find().populate("author").populate("genre")
        res.send(books);
    }
    catch (e) {
        console.log("Error in get books + " + e)
        res.status(404).send(e)
    }
})

router.get("/:id?", async (req, res) => {
    try {
        const books = await Book.find({ _id: req.params.id }).populate("author").populate("genre")
        res.send(books);
    }
    catch (e) {
        console.log("Error in get books by id + " + e)
        res.status(404).send(e)
    }
})
router.post("/", async (req, res) => {
    try {
        const books = await Book.find({ title: req.body.title }).populate("author").populate("genre")
        res.send(books);
    } catch (e) {
        console.log("Error in post books + " + e);
        res.status(404).send(e)
    }
})

router.put("/:id?", async (req, res) => {
    try {
        await Book.findByIdAndUpdate(req.params.id, req.body, function (err, doc) {
            if (err) return res.send(500, { error: err })
            return res.send("Changed succesfully")
        })
    } catch (e) {
        console.log("Error in put books + " + e);
        res.status(404).send(e)
    }
})

router.delete("/:id?", async (req, res) => {
    try {
        await Book.findByIdAndDelete(req.params.id, function (err, doc) {
            if (err) return res.send(500, { error: err })
            return res.send("Deleted succesfully")
        })
    } catch (e) {
        console.log("Error in delete books + " + e);
        res.status(404).send(e)
    }
})


module.exports = router