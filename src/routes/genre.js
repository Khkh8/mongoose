const router = require("express").Router()
const Genre = require("../models/Genres")

router.get("/", async (req, res) => {
    try {
        const genres = await Genre.find().lean()

        res.send(genres)
    }
    catch (e) {
        console.log("Error in get genres + " + e)
        res.status(404).send(e)
    }
})

router.get("/:id?", async (req, res) => {
    try {
        const genres = await Genre.find({ _id: req.params.id }).lean()
        res.send(genres);
    } catch (e) {
        console.log("Error in get genres by id + " + e);
        res.status(404).send(e)
    }

})

router.post("/", async (req, res) => {
    try {
        const genres = await Genre.find({ name: req.body.name }).lean()
        res.send(genres);
    } catch (e) {
        console.log("Error in post genres + " + e);
        res.status(404).send(e)
    }
})

router.put("/:id?", async (req, res) => {
    try {
        await Genre.findByIdAndUpdate(req.params.id, req.body, function (err, doc) {
            if (err) return res.send(500, { error: err })
            return res.send("Changed succesfully")
        })
    } catch (e) {
        console.log("Error in put genres + " + e);
        res.status(404).send(e)
    }
})

router.delete("/:id?", async (req, res) => {
    try {
        await Genre.findByIdAndDelete(req.params.id, function (err, doc) {
            if (err) return res.send(500, { error: err })
            return res.send("Deleted succesfully")
        })
    } catch (e) {
        console.log("Error in delete genres + " + e);
        res.status(404).send(e)
    }
})

module.exports = router