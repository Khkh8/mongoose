const router = require("express").Router()
const BookInstance = require("../models/BookInstances")

router.get("/", async (req, res) => {
    try {
        const bookinstances = await BookInstance.find().populate("book")
        console.log(bookinstances);
        res.send(bookinstances);
    }
    catch (e) {
        console.log("Error in get bookInstances + " + e)
        res.status(404).send(e)
    }
})

router.get("/:id?", async (req, res) => {
    try {
        const bookInstances = await BookInstance.find({ _id: req.params.id }).lean()
        res.send(bookInstances);
    }
    catch (e) {
        console.log("Error in get bookInstances by id + " + e)
        res.status(404).send(e)
    }
})
router.post("/", async (req, res) => {
    try {
        const bookInstances = await BookInstance.find({ status: req.body.status }).populate("author").populate("genre")
        res.send(bookInstances);
    } catch (e) {
        console.log("Error in post bookInstances + " + e);
        res.status(404).send(e)
    }
})

router.put("/:id?", async (req, res) => {
    try {
        await BookInstance.findByIdAndUpdate(req.params.id, req.body, function (err, doc) {
            if (err) return res.send(500, { error: err })
            return res.send("Changed succesfully")
        })
    } catch (e) {
        console.log("Error in put bookInstances + " + e);
        res.status(404).send(e)
    }
})

router.delete("/:id?", async (req, res) => {
    try {
        await BookInstance.findByIdAndDelete(req.params.id, function (err, doc) {
            if (err) return res.send(500, { error: err })
            return res.send("Deleted succesfully")
        })
    } catch (e) {
        console.log("Error in delete bookInstances + " + e);
        res.status(404).send(e)
    }
})


module.exports = router