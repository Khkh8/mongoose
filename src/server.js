require("dotenv").config()
const express = require("express")
const port = process.env.PORT
const app = express()
const bodyParser = require("body-parser")
const mongoose = require("mongoose")
const routes = require("./routes") // hakee automaattisesti indexin

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

mongoose.connect(process.env.CONNECTION_STRING, {useNewUrlParser: true})
const db = mongoose.connection

db.on("error", console.error.bind(console, "connection error:"))

db.once("open", () => {
    console.log("Database connection established...");
})

app.use("/api/v1/genres", routes.genre)
app.use("/api/v1/books", routes.book)
app.use("/api/v1/authors", routes.author)
app.use("/api/v1/bookInstances", routes.bookinstance)

app.get("/", (req, res) => {
    res.send("Local Library")
})

app.listen(port, () => {
    console.log(`Server running at port ${port}...`)
})