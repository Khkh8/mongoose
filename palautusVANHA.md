# Mongoose assignment
### Henri Vatto

## Mongo schemas
```javascript
const Author = new mongoose.Schema({
    first_name: String,
    family_name: String,
    date_of_birth: Date,
    date_of_death: Date,
    
    name: String,
    lifespan: String
})
```
```javascript
const BookInstance = new mongoose.Schema({
    book: Book,
    imprint: String,
    status: Enumerator,
    due_back: Date
})
```
```javascript
const Book = new mongoose.Schema({
    title: String,
    author: Author,
    summary: String,
    ISBN: String,
    genre: Genre
})
```
```javascript
const Genre = new mongoose.Schema({
    name: String
})
```
```javascript
app.get('/', (req, res) => {
  res.send('hello world')
})
```
## screenshots
![asd](./images/image.PNG)
![dsa](./images/image2.png)